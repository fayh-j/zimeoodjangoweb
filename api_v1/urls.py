from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from api_v1 import views
from rest_framework_simplejwt import views as jwt_views

router = routers.DefaultRouter()


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    #path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('', views.indexView.as_view(), name='welcom'),
    path('partner', views.indexView.as_view(), name='welcom'),
    path('welcom/siteNb', views.siteNb ),
    path('welcom/portfolioNb', views.portfolioNb ),
    path('welcom/commandNb', views.commandNb ),
    path('welcom/date', views.date),

    path('user/login/token', jwt_views.TokenObtainPairView.as_view(), name='get_token'),
    path('user/login/token/refresh', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('user/register', views.registerView.as_view() ),
    path('user/check_auth', views.checkAuthView.as_view(), name='check_auth'),
    path('user/dashboard', views.dashboardView.as_view(), name='dashboard'),
    path('user/logout', views.logoutView.as_view()),
    path('user/whatsappSuscribe', views.whatsappView.as_view()),
    path('user/partner/become', views.partnerView.as_view()),
    path('user/partner/checkIfExist', views.partnerView.as_view()),
    path('user/partner/childs', views.partnerChildsView.as_view()),
    path('user/partner/paids', views.partnerPaidListView.as_view()),

    path('site', views.siteView.as_view()),
    path('site/site_templates', views.sites_templatesView.as_view()),
    path('site/webpage', views.webPagesView.as_view()),
    path('site/images', views.getImgsViews.as_view()),
    path('site/images/delete', views.getImgsViews.as_view()),
    path('site/domain', views.domainView.as_view()),
    path('site/update', views.siteUpdateView.as_view()),

    path('domain', views.domainView.as_view()),
    path('domain/checkIfExist', views.checkDomainView.as_view()),
    
    path('uploads/form', views.fileUploadView.as_view()), 
    path('uploads', views.fileUploadView.as_view()),    
    
    #path('login', views.LoginView.as_view(), name='login'),
]
