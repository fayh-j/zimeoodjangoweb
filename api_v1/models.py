from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



class Site_category(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    description = models.TextField(null=True)
    created_at = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")

    class Meta:
        verbose_name = "website category"
        ordering = ['name']

    def __str__(self):
        return self.name

class Whatsapp(models.Model):
    user = models.ForeignKey(User, null=True, on_delete = models.PROTECT)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")
    phone = models.IntegerField(default=0)
    class Meta:
        verbose_name = "whatsapp"
        ordering = ['user']
    
    def __str__(self):
        return self.user.username

class Partner(models.Model):
    user = models.OneToOneField(User, null=True, on_delete = models.PROTECT)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")
    money= models.IntegerField(default=0)
    class Meta:
        verbose_name = "partner"
        ordering = ['user']
    
    def __str__(self):
        return self.user.username


class Profile(models.Model):
    user = models.OneToOneField(User, null=True, on_delete = models.PROTECT) 
    username =  models.CharField(max_length=100,  default = "user.username")
    parent = models.ForeignKey(Partner, null=True, on_delete = models.PROTECT) 
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")
    class Meta:
        verbose_name = "profile"
        ordering = ['user']
    
    def __str__(self):
        return self.user.username
    

class Tag(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    description = models.TextField(default="Un modèle responsif qui vous correspond")
    created_at = models.DateTimeField(default=timezone.now,     verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de supression")
    class Meta:
        verbose_name = "tags"
        ordering = ['name']
    
    def __str__(self):
        return self.name


class Site_template(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    templateName = models.CharField(max_length=100, default="Template name")
    description = models.TextField(default="Un modèle responsif qui vous correspond")
    site_category = models.ForeignKey(Site_category, null=True, on_delete = models.PROTECT)
    #style = models.IntegerField(default=1, null=True)
    created_at = models.DateTimeField(default=timezone.now,     verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de supression")
    class Meta:
        verbose_name = "website template"
        ordering = ['name']
    
    def __str__(self):
        return self.name

class Site_template_has_tag(models.Model):
    site_template  = models.OneToOneField(Site_template, null=True, on_delete = models.PROTECT)
    tag = models.OneToOneField(Tag, null=True, on_delete = models.PROTECT)
    created_at = models.DateTimeField(default=timezone.now,     verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de supression")
    class Meta:
        verbose_name = "template tags"
    
    def __str__(self):
        return self.site_template.name+ " "+ self.tag.name

class Site(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, default="Modifiez facilement le contenu de votre site gratuit. Créer un site web n'a jamais été aussi simple.")
    price = models.IntegerField(default=0)
    slug = models.SlugField(max_length=100, default="")
    site_type = models.CharField(max_length=10, default="free")#can be "free" or "command"
    created_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now, verbose_name="Date de supression")
    site_template = models.ForeignKey("Site_template",  null=True, on_delete=models.CASCADE)
    user = models.ForeignKey(User, null=True, on_delete = models.PROTECT)
    cahier_charge_path = models.CharField(default="",  null=True, max_length=300)

    class Meta:
        verbose_name = "website"
        ordering = ['name']
    
    def __str__(self):
        return self.name


class File(models.Model):
    data = models.FileField(upload_to='files/')
    uploaded_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")


class Domain(models.Model):
    name = models.CharField(max_length=100,  unique=True)
    tld = models.CharField(max_length=10)
    payment_date = models.DateTimeField(default=timezone.now,   verbose_name="next payment date")
    user = models.ForeignKey(User, null=True , on_delete = models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")
    #renouvellment = 
    #deleted_at = models.DateTimeField(verbose_name="Date de supression")
    site = models.ForeignKey('Site', null=True, on_delete = models.CASCADE)

    class Meta:
        verbose_name = "website domain"
        ordering = ['name']

    def __str__(self):
        return self.name

class ImageUrl(models.Model):
    url = models.CharField(max_length=500,  unique=True)
    user = models.ForeignKey(User, null=True, on_delete = models.PROTECT)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")
    

class PartnerGetMoney(models.Model):
    partner = models.OneToOneField(Partner, null=True, on_delete = models.PROTECT)
    money= models.IntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now,   verbose_name="Date de création")
    updated_at = models.DateTimeField(default=timezone.now,  verbose_name="Date de modification")

    class Meta:
        verbose_name = "Patner Paids"

    def __str__(self):
        return self.partner.user.username