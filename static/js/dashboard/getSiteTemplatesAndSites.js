$(function(){
    console.log('zimeooUser : '+$.cookie('zimeooUser'));
    $('.loadingSite').removeClass('invisible')
    $('#home').removeClass('active');
    $('#showDashboard').addClass('active');
          //alert('baba');
    var SiteTemplatesAndSitesObject = {
      executeFunction: function(){
        console.log('in   getSiteTemplatesAndSites()')
        getSiteTemplates()
        getSites()
        getChilds()
        getPaids()
        $('.loadingSite').addClass('invisible')
      } 
    } 
  
    checkAuth(SiteTemplatesAndSitesObject)
         
});

function getSiteTemplates(){
      $.ajax({
          url: "/v1.0/site/site_templates",
          method: "GET",
          headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
          datatype: "json"
      }).done(function(response) {
        //alert('yeah');
        //$.cookie('zimeooTemplates', response , { expires: 200 });
        $.cookie("zimeooTemplates", JSON.stringify(response));
        var limit = 20;
        //console.log( JSON.parse($.cookie('zimeooTemplates'))[0]);
        if (limit > response.length  ) limit = response.length;
        for (i=0; i< limit ; i++){
          
          var data = "/static/img/templates/"+ response[i].fields.name +".png";
          var type = response[i].fields.name.split('_')[0];
          var description = response[i].fields.description;
          var templateName = response[i].fields.templateName;

          //console.log(data);

          $('#itemTest').after('<div class="card col-md-6 col-12  item '+type+' " style="border: 0px;"> <img  data-toggle="modal" data-target="#siteModal"  id="'+response[i].fields.name +'"  class="card-img-top  bounce_button" style="border-radius: 5px; margin-bottom: 10px; " src="' + 
          data + '" alt="Card image cap"> <table style="border: 0; margin-top: 15px"> <td><tr><span style=" font-weight: bold; font-size: 1.2em; " >'+ templateName+'</span></tr> <tr><span style=" font-size: 0.8em; color: rgba(68, 68, 68, 0.75);" >'+ description+'</span></tr> </td></table></div>');
        }
        $('#itemTest').remove();


        $('.CreateSite2 .card img').bind('click', function(){
              $('#siteModal .choice').val($(this).prop("id"));
              $('#siteModal .see').val($(this).prop("id"));
        });
      });
}

function getSites(){
  var  request;
  var data = {username: $.cookie('zimeooUser')};
  request = $.ajax({
      url: "/v1.0/site",
      method: "GET",
      headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
      data: data,
      datatype: "json"
  });
  request.done(function(response) {
    if (response.length==0) $('.noSite').removeClass('invisible');
    for (i=0; i< response.length ; i++){
      var domains = "domina";
      
          $('.noSite').remove();
          var state= null;
          console.log("suite");
          if( response[i].domains != "" ){
            console.log(" domains : "+ response[i].domains);
            var domainsJson = response[i].domains
            var dstate = "active"
            for (d=0; d< domainsJson.length; d++){
              var att= "disabled"; var attv = "Domaine actif"
              console.log("domainsJson[d].state : "+domainsJson[d].state)
              if (domainsJson[d].state == "inactive"){
                dstate = "inactive"; att = ""; attv="Renouveller"
              } 
              $('#siteStateModal #domains').append('<tr>'+
              '<td class="text-center">'+ domainsJson[d].fields.name+'.'+domainsJson[d].fields.tld +'</td>'+
              '<td class="text-center">'+ domainsJson[d].fields.payment_date.substring(0, 10)+'</td>'+
              '<td class="text-center"><button class="btn btn-outline-primary renewDomain" id ="'+domainsJson[d].pk+'" '+att+' >'+attv+'</button> </td></tr>')
            }
            if( dstate== "inactive"){
              $('#infoNoDomain').removeClass('invisible')
              state = $('<a href="#"  data-toggle="modal" data-target="#siteStateModal" style="width: 100%" class="btn btn-danger" id="'+i+'" >Domaines <i class="fa fa-globe"></i></a>');
            } 
            else {
              console.log('inactif')
              state = $('<a href="#"  data-toggle="modal" data-target="#siteStateModal" style="width: 100%" class="btn btn-success" id="'+i+'" >Domaines <i class="fa fa-globe"></i></a>');
            } 
          }else{console.log("nooo");
            state = $('<a href="#"  style="width: 100%" class="btn btn-outline-danger addDomainModal" id="'+response[i].pk+'_'+response[i].fields.name+'" >Ajouter un Nom de domain <i class="fa fa-globe"></i></a>');
          }
          var card= $('<div class="mycard col-md-4 col-12" > <div class="card-body"><h4 class="card-title">'+response[i].fields.name+'</h4><p class="card-text">'+response[i].fields.description+'</p> <hr class="toAdd"><hr><a href="#"   style="width: 100%" class="btn btn-warning detailModal" id='+"'"+$.cookie('zimeooDomainSite'+i)+"'"+'>Détails <i class="fa fa-eye"></i></a><hr> <a href="#" id='+response[i].pk+'  style="width: 100%" class="btn btn-info modifySite">Modifier <i class="fa fa-edit"></i></a> <hr> <a href="#" id="'+response[i].pk+'"  style="width: 100%" class="btn btn-dark deleteSite" data-target="#deleteSiteModal"  data-toggle="modal">Suppimer <i class="fa fa-trash"></i></a> </div> </div>');
          card.find('.toAdd').after(state)
          $('.siteList .itemTest').after(card);
         
        }
        $('.detailModal').click(function(e){
          e.preventDefault();
          if($(this).attr('id')!= "undefined"){
            var json=  JSON.parse( $(this).attr('id'));
            console.log("After domains :"+ json);
            var details = "Votre site est actif avec les domaines suivants : ";
            for (i=0; i< json.length; i++){
              details+= '<br> &nbsp&nbsp&nbsp&nbsp&nbsp-&nbsp <strong>'+
              json[i].fields.name+'.'+json[i].fields.tld+',&nbsp activé le '+json[i] .fields.payment_date.split('T10')[0] +' </strong>';
            }
            $('#detailModal .modal-body').html(details);
          }
          
            $('#detailModal').modal('show');
        });
        $('.addDomainModal').click(function(e){
          e.preventDefault();
          var dn0 = $(this).attr('id').split('_') 
          var dn= dn0[1].replace(' ','');
          $('#addDomainModal #domainNameInput').val( dn );
          $('#addDomainModal #domainName').val( dn+$('.tld').val().split('_')[0] );
          $('#addDomainModal #price').text($('#addDomainModal .tld').val().split('_')[1]);
          $('#addDomainModal .next').attr("id", dn0[0]  )
          $('#addDomainModal').modal('show');
        });
        $('#domainNameInput, .tld').bind('focus change input', function(){
     var write = $('#addDomainModal #domainNameInput').val().toLowerCase();
     write = write.split(' ').join('')
          $('#addDomainModal #domainName').val( write +$('.tld').val().split('_')[0]  );
          $('#addDomainModal #price').text($('#addDomainModal .tld').val().split('_')[1]);
        });

        $('.mycard .card-body').css('border','1px solid rgba(0, 0, 0, 0.1)').css('border-radius','5px').css('box-shadow','0px 4px 8px rgba(34,82,193, 0.5)');
        $('.mycard .card-body').bind('mouseenter', function(){
          $(this).css('box-shadow','0px 10px 25px rgba(34,82,193, 0.8)');
        }).bind('mouseleave', function(){
          $(this).css('box-shadow','0px 4px 8px rgba(34,82,193, 0.5)');
        });
        $('.deleteSite').bind('click', function(e){
          
          $('#deleteSiteModal .conf').val($(this).attr("id"));
        });
      
      
      $('.modifySite').bind('click', function(){
        $.cookie('zimeooCurrentSite', $(this).attr("id") , { expires: 20000 });
        var site_id = $(this).attr('id');
          var data = {
                  id: site_id ,
                };
        getWebPages(data, site_id)
        getImages(data, site_id)
          
          $('#modifySiteModal').modal('show');
          
      });
     
  });

  $('#reloadImg').click(function(){
    $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp  &nbsp Actualisation...')
    reloadImages()
  })

  $('#uploadImg').click(function () { 
    $('#modifySiteModal').modal('hide')
    $('#addImgModal').modal('show')
  })



  $('#infoNoDomain').addClass('invisible')
  $('.infoNoDomain').click(function(e){
    e.preventDefault();
    $('#infoNoDomain, #infoDomainName').remove();
  });
  $('#addDomainModal .next').click(function(){
    if($('#addDomainModal .step1').hasClass('invisible')){

    }else{
      $('#addDomainModal input, #addDomainModal select, #addDomainModal .next').attr("disabled", true);
      $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp Traitement...')
      $.ajax({
        url: "/v1.0/site/domain",
        method: "POST",
        data: {name: $('#domainName').val().split('.')[0] , tld: $('#domainName').val().split('.')[1] , siteID: $(this).attr('id')},
        headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
      }).done(function( data ) {
        $('#addDomainModal .next').html('Continuer')
        $('#addDomainModal input, #addDomainModal select, #addDomainModal .next').attr("disabled", false);
        window.location.href = '/v1.0/user/dashboard';
      }).fail(function(xhr, status, error) {
          console.log('failed')
          $('#addDomainModal .next').html('Continuer')
          $('#addDomainModal input, #addDomainModal select, #addDomainModal .next').attr("disabled", false);
      });
    }
  });
  $('#addDomainModal .stop').click(function(){
    $('#addDomainModal input, #addDomainModal select, #addDomainModal .next').attr("disabled", false);
    $('#addDomainModal').modal('hide');
      
  });

}

  
function getWebPages(data, site_id){
  $.ajax({
    url: "/v1.0/site/webpage",
    method: "GET",
    headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
    data: data,
    datatype: "json"
  }).done(function(response) {
    $('#modifySiteModal #webpages').html('');
    $('.loadingWebPages').addClass('invisible');
    response = response.split(',')
    for(i = response.length-1; i>=1; i--){
      var wp= response[i]
      var name = wp.split('.')[0]
      console.log('i= '+i)
      if (wp.split('.')[0]=="index" ) name = "Accueil"
      
      $('#modifySiteModal #webpages').append('<tr>'+
              '<td scope="row" class="text-center">'+name+'</td>'+
              '<td class="text-center"><button id="site_'+site_id+'/distribution/'+wp+'" class="btn btn-success modifySiteButon" >Modifier</button></td>'+
            '</tr>');
            $('.modifySiteButon').click(function(){
    
          var link = '/static/zimeooUserSites/'+$(this).attr("id")+'?'+$.cookie('zimeooUser')+'&'+$.cookie('zimeooUserToken')+'&'+$.cookie('zimeooUserRefresh'); ;
        
          var win = window.open( link, '_blank');
          if (win) win.focus();
          
        });
    }
  });
  $('#addImgModal , #deleteModal').on('hidden.bs.modal', function (e) {
    $('#addImgModal #upload').html('Uploader').attr('disabled', false)
    reloadImages()
    $('#modifySiteModal').modal('show')
  })
}

function getImages(data, site_id){
  $.ajax({
    url: "/v1.0/site/images",
    method: "GET",
    headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
    data: data,
    datatype: "json"
  }).done(function(response) {
  
  $('#modifySiteModal #images').html('');
  images = response.split('\n')
  //console.log('images '+images)
  for(i = 1; i< images.length-1;  i++){
    var img= images[i]
    //console.log('image '+i+" : "+img)
    var deleteButton = $('<td class="text-center"><button id="'+img+'" class="btn btn-outline-danger deleteImgButton" >Supprimer &nbsp <i class="fa fa-trash"></i></button></td>')
    var name = img.split('/')[img.split('/').length-1].split('&&&id')[0]
    var line= $('<tr>'+
    '<td scope="row" class="text-center">'+name +'</td>'+
    '<td class="text-center"><img src="/'+img.split('&&&id')[0]+'" class="img-fluid" style="max-height: 150px" ></td></tr>')
    line.append(deleteButton)
    $('#modifySiteModal #images').append(line);
    $('.deleteImgButton').bind('click' , function(e){
      e.preventDefault()
      $('#modifySiteModal').modal('hide')
      $('#deleteModal').modal('show')
      $('#deleteModal .conf').attr('id', $(this).attr('id'))
    })
        $('.seeImgButton').click(function(){
            $('#siteModal').modal('hide');
            $('#seeImgModal').modal("show").find('img').attr('src',  $(this).attr('id'))
          });
    }
    
  $('.loadingImages').addClass('invisible');
  });
}

function reloadImages(){
  var site_id = $('.modifySite').attr('id');
          var data = {
                  id: site_id ,
                };
        getImages(data, site_id)
    $("#reloadImg").html('Actualiser')
}
function getChilds(){

    $.ajax({
          url: "/v1.0/user/partner/childs",
          method: "GET",
          headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
          datatype: "json"
      }).done(function(response) {
        $('.loadingChilds').addClass('invisible')
        for (var i =response.length-1  ; i>=0 ; i--){
          var child = response[i]
          var  paid = "Non"
          if (child.fields.nbDomains >0 ) paid = "Oui"
          $('#partnerModal #childs').append('<tr>'+
              '<td scope="row" class="text-center">'+child.fields.username+'</td>'+
              '<td class="text-center"> Le'+child.fields.created_at.split('T')[0] + ' à '+child.fields.created_at.split('T')[1]+'</td>'+
              '<td class="text-center">'+child.fields.nbDomains +'</td>'+
              '<td class="text-center">'+  paid +'</td>'+
            '</tr>');
        }
      }).fail(function(xhr, status, error) {
        $('.loadingChilds').addClass('invisible')
      });;
}

function getPaids(){
    $.ajax({
          url: "/v1.0/user/partner/paids",
          method: "GET",
          headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
          datatype: "json"
      }).done(function(response) {
        $('.loadingPaids').addClass('invisible')
        console.log(' response : '+ response)
        for (var i =response.length-1  ; i>=0 ; i--){
          var paid = response[i]
          $('#partnerModal #paids').append('<tr>'+
              '<td scope="row" class="text-center">'+paid.fields.money+' FCFA</td>'+
              '<td class="text-center"> Le'+paid.fields.created_at.split('T')[0] + ' à '+paid.fields.created_at.split('T')[1]+'</td>'+
            '</tr>');
        }
      }).fail(function(xhr, status, error) {
        $('.loadingPaids').addClass('invisible')
      });;
}