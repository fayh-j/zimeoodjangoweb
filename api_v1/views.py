import os, io, json, datetime
from datetime import timedelta
from api_v1.models import *
from api_v1.forms import *


from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.hashers import make_password, check_password
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage, FileSystemStorage
from django.core.exceptions import ObjectDoesNotExist
from django.core import serializers
from django.conf import settings
from django.http import  HttpResponse, HttpResponseRedirect, JsonResponse
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime, timedelta

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import *
from rest_framework.status import (HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_200_OK)
from rest_framework.response import Response



class indexView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        error = False
        loginForm = ConnexionForm()
        registerForm = RegisterForm()
        userUpdateForm = UserUpdateForm()

        return render(request, 'api_v1/index.html', locals())


class registerView(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        if (request.POST.get("type") =="register"): #if register request
            registerForm = RegisterForm(request.POST)

            if registerForm.is_valid():
                firstname = registerForm.cleaned_data['firstname2']
                lastname = registerForm.cleaned_data['lastname2']
                username = registerForm.cleaned_data['username2']
                password = registerForm.cleaned_data['password2']
                password_conf = registerForm.cleaned_data['password_conf2']
                partner = request.POST.get('partner')
                
                try:
                    user = User.objects.get(username= username)
                except User.DoesNotExist:
                    user= None
                
                if user:  
                    return HttpResponse("{0}".format("Error_Cette adresse email est déjà utilisée !"))
                else:
                    newUser = User(username= username, first_name= firstname, last_name = lastname, email= username)
                    newUser.password =  make_password(password)
                    newUser.save()
                    if  partner == "":  
                        userProfile = Profile(user = newUser, username = username)
                    else: 
                        userProfile = Profile(user = newUser, username = username, parent = Partner.objects.get(  user = User.objects.get(username=partner) )  )
                    userProfile.save()
                    return HttpResponse("{0}".format("success"))
            else :
                return HttpResponse("{0}".format(registerForm.errors))
        
        elif (request.POST.get("type") =="google"):
            username = request.POST.get('username')
            partner = request.POST.get('partner')
            try:
                user = User.objects.get(username= username)
                login(request, user)
                return  HttpResponse("{0}".format("login"))
            except User.DoesNotExist:
                newUser = User(username= username)
                newUser.password =  make_password("zimeooGoogleAuth")
                newUser.save()
                if  partner == "":  
                    userProfile = Profile(user = newUser, username = username)
                else: 
                    userProfile = Profile(user = newUser, username = username, parent = Partner.objects.get(  user = User.objects.get(username=partner) )  )
                userProfile.save()
                login(request, newUser)
                return  HttpResponse("{0}".format("signUp"))
            #return  HttpResponse("{0}".format("success"))

        username = request.data.get("username")
        password = request.data.get("password")


class dashboardView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        fileForm = FileForm()
        userUpdateForm = UserUpdateForm()
        #userUpdateForm = UserUpdateForm(request.POST)
        return render(request, 'api_v1/dashboard.html', locals())


class userView(APIView):
    permission_classes = (IsAuthenticated,)
    def update(self, request):
        user = request.user
        user.set_password(request.POST["password"])
        user.email = request.POST["email"]
        user.firstname =request.POST["firstname"]
        user.lastname =request.POST["lastname"]
        user.save()
        return Response({'status': 'success'}, status=HTTP_200_OK)

        
class checkAuthView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        qs =  Whatsapp.objects.filter( user = User.objects.get( username=request.user.username).pk ).count()
        if qs >0: return Response({'status': 'yes'}, status=HTTP_200_OK)
        else :  return Response({'status': 'no'}, status=HTTP_200_OK) 

        
class whatsappView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        w=  Whatsapp( user=User.objects.get(username=request.user.username) , phone=request.POST.get('phone') )
        w.save()
        return Response({'status': 'success'}, status=HTTP_200_OK)


class logoutView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        logout(request)
        return Response({'status': 'logout'}, status=HTTP_200_OK)


class sites_templatesView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        """
        qs = Site_template.objects.filter(name__contains=request.POST.get('type'))
        qs_json = serializers.serialize('json', qs)
        """
        qs = Site_template.objects.all().order_by('?')
        qs_json = serializers.serialize('json', qs)
        return HttpResponse(qs_json, content_type='application/json')


class siteView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        qs = Site.objects.filter(user__username=request.GET.get('username'))
        qs_json = serializers.serialize('json', qs)
        #return HttpResponse(qs_json, content_type='application/json')
        sites = eval(qs_json)
        for site in sites:
            d = Domain.objects.filter(site__id__contains= site['pk'] )
            domains_json = serializers.serialize('json', d)
            domains = eval(domains_json)
            for domain in  domains:
                payment_date = datetime.strptime( domain["fields"]["payment_date"][0:10], '%Y-%m-%d' )
                present = datetime.now()
                if payment_date >= present: domain["state"] = "active"
                else: domain["state"] = "inactive"
                #after = datetime.now() + timedelta(days=3)

            site['domains']= domains
            #return HttpResponse("{0}".format(site))
        
        return HttpResponse(json.dumps(sites) , content_type='application/json')
        #return HttpResponse("{0}".format(Site_template.objects.filter(site_category__name__contains=request.POST.get('type'))))

    def post(self, request):
        result = "success"
        user = User.objects.get(username=request.POST.get('username'))
        if request.POST.get('type') == "free":
            name = request.POST.get('template')
            template =  Site_template.objects.get( name =name) 
            site= Site( name = request.POST.get('siteName'), user= user, site_template = template, site_type="free"   )
            site.save()
            #create Site folder
            id = str(site.id)
            myCmd1 = os.popen('cd static/zimeooUserSites  & mkdir  static/zimeooUserSites/site_'+ id+' & ls' ).read()
            myCmd2 = os.popen('cp -r  static/templates/'+name+'/distribution   static/zimeooUserSites/site_'+id + '/  &  cd static/zimeooUserSites/site_'+id  ).read()
            myCmd3 = os.popen('cp  static/js/manageSite.js   static/zimeooUserSites/site_'+id + '/distribution/js ' ).read()
            myCmd3 = os.popen('cp  static/css/manageSite.css   static/zimeooUserSites/site_'+id + '/distribution/css ' ).read()

            myCmd4 = os.popen('cp  static/js/jquery.editable.min.js   static/zimeooUserSites/site_'+id + '/distribution/js ' ).read()

            myCmd5 = os.popen('cp  static/js/jquery.cookie.js   static/zimeooUserSites/site_'+id + '/distribution/js ' ).read()
            return HttpResponse("{0}".format(name))
            return HttpResponse("{0}".format("success"))
        else:
            description =  request.POST.get('description')
            site= Site( name = request.POST.get('siteName'), user= user, description=description , site_type="command"   )
            site.save()
            #create Site folder
            id = str(site.id)
            myCmd1 = os.popen('cd static/zimeooUserSites  & mkdir site_'+ id+' & ls' ).read()
            return HttpResponse("{0}".format(myCmd1))

    def update(self, request):
        #return HttpResponse("{0}".format("ok"))
        newSite = '<!DOCTYPE html>\n<html>\n'+request.POST.get('site')+'\n</html>'
        #return HttpResponse("{0}".format(newSite))
        d = str(datetime.today()).replace(' ','___').split('.')[0]
        id =request.UPDATE.get('siteID')
        
        myCmd1 = os.popen('cd static/zimeooUserSites/site_'+ str(id)+'/distribution/ & mkdir -p backup & mv index.html  backup/index___'+d+'.html  ' ).read()
        #f= open("static/zimeooUserSites/site_"+ str(id)+"/distribution/index.html","w+")
        fname=  "static/zimeooUserSites/site_"+ str(id)+"/distribution/index.html"
        with io.open(fname, "w", encoding="utf-8") as f:
            f.write(newSite)
            f.close

        return HttpResponse("{0}".format("success"))


    def delete(self, request):
        #return HttpResponse("{0}".format(request.POST.get('siteID')))
        s = Site.objects.get(id=request.POST.get('siteID')).delete()
        id =request.POST.get('siteID')
        myCmd1 = os.popen(' rm -r static/zimeooUserSites/site_'+ str(id) ).read()
        return HttpResponse("{0}".format("success"))


class siteUpdateView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        #return HttpResponse("{0}".format("ok"))
        newSite = '<!DOCTYPE html>\n<html>\n'+request.POST.get('site')+'\n</html>'
        #return HttpResponse("{0}".format(newSite))
        d = str(datetime.today()).replace(' ','___').split('.')[0]
        id =request.POST.get('siteID')
        
        myCmd1 = os.popen('cd static/zimeooUserSites/site_'+ str(id)+'/distribution/ & mkdir -p backup & mv index.html  backup/index___'+d+'.html  ' ).read()
        #f= open("static/zimeooUserSites/site_"+ str(id)+"/distribution/index.html","w+")
        fname=  "static/zimeooUserSites/site_"+ str(id)+"/distribution/index.html"
        with io.open(fname, "w", encoding="utf-8") as f:
            f.write(newSite)
            f.close

        return HttpResponse("{0}".format("success"))


class domainView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        qs = Domain.objects.filter(site__id__contains=request.GET.get('sitePk'))
        qs_json = serializers.serialize('json', qs)
        return HttpResponse(qs_json, content_type='application/json')

    def post(self, request):
        after = datetime.now() + timedelta(days=365)
        domain = Domain(name= request.POST.get('name'), user= request.user,  tld=request.POST.get('tld'), site=Site(pk=request.POST.get('siteID')), payment_date=after)
        
        domain.save()
        return HttpResponse("{0}".format("success"))
        #except:
        #    return HttpResponse("{0}".format("error"))


class webPagesView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        import os
        id= request.GET.get('id')
        myCmd = os.popen('ls  static/zimeooUserSites/site_'+id+'/distribution ' ).read()
        files = myCmd.split('\n')
        webPages= ""
        for f in files: 
            if ('.html' in f): webPages += ','+f
        
        return  HttpResponse("{0}".format(webPages))


class siteStyleView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        siteId = str(request.POST.get('siteId'))
        import operator
        colors = {}
        with open('test2.css') as f:
            line = f.readline()
            for i in range(len(line)):
                letter = line[i]
                if letter == "#":
                    color = ""
                    j=i
                    while j < len(line) and line[j]!= ";"  and  line[j]!= "}" and  line[j]!= " " and  line[j]!= "." and  line[j]!= "{" and  line[j]!= ":" :
                        color += line[j]
                        j+=1
                    if len(color)<=7  :
                        if color in colors:
                            colors[color]+=1
                        else:
                            colors[color]=1
                        #colors.append(color)
                if i< len(line)+10 and  ( line[i:i+4]=='rgb(' or line[i:i+5]=='rgba(' ):
                    color = ""
                    j=i
                    while j < len(line) and line[j]!= ")"  :
                        color += line[j]
                        j+=1
                    if 1==1  :
                        if color in colors:
                            colors[color]+=1
                        else:
                            colors[color]=1
                                            
        colorsS = sorted( colors.items(), key=operator.itemgetter(1), reverse = True ) 
        print(colorsS)
        
        return  HttpResponse("{0}".format(webPages))



class getImgsViews(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        result = ''
        qs = ImageUrl.objects.filter(user=request.user)
        qs_json = serializers.serialize('json', qs)
        oimgs = eval(qs_json)
        for im in oimgs: result+= '\n'+ str( im['fields']['url'] )+ '&&&id='+str(im['pk'])

        id= request.GET.get('id')
        q = str(os.popen('ls  static/zimeooUserSites/site_'+id + '/distribution/img/' ).read()).split('\n')
        for path in  q: 
            t = path.split('/') 
            tlen = len(t) 
            if '.' in t[tlen-1]:
                result += '\nstatic/zimeooUserSites/site_'+id + '/distribution/img/' +path
        result+= '\n'

        """q = str(os.popen('ls  static/zimeooUserSites/site_'+id + '/distribution/img/*.jpg ' ).read()).split('\n')
                                for path in  q: result += '\nstatic/zimeooUserSites/site_'+id + '/distribution/img/' +path
                        
                                q = str(os.popen('ls  static/zimeooUserSites/site_'+id + '/distribution/img/*.PNG ' ).read()).split('\n')
                                for path in  q: result += '\nstatic/zimeooUserSites/site_'+id + '/distribution/img/' +path
                        
                                q = str(os.popen('ls  static/zimeooUserSites/site_'+id + '/distribution/img/*.JPG ' ).read()).split('\n')
                                for path in  q: result += '\nstatic/zimeooUserSites/site_'+id + '/distribution/img/' +path
                        """
        return HttpResponse("{0}".format(result))
    
    def delete(self, request):
        path = str(request.POST.get('path'))
        if "&&&id=" in  path:
            spl = path.split('&&&id=')
            id = spl[ len(spl)-1 ]
            ImageUrl.objects.filter(pk=id).delete()
            q = os.popen('rm  '+ spl[ 0]   ).read()
            return HttpResponse("{0}".format("success"))
        else:
            q = os.popen('rm  '+ path  ).read()
            return HttpResponse("{0}".format("no"))


class checkDomainView(APIView):
    permission_classes = (AllowAny,)
    def get(self, request):
        import os
        domain= request.GET.get('domain')
        myCmd = os.popen('ping -c 4 '+domain ).read()
        myCmd = myCmd.split(' ping statistics --- ')
        
        return  HttpResponse("{0}".format(myCmd))


@csrf_exempt
def siteNb(request):
    qs = Site.objects.all()
    return HttpResponse(len(qs))

@csrf_exempt
def portfolioNb(request):
    qs = Site.objects.filter(site_type='free')
    return HttpResponse(len(qs))

@csrf_exempt
def commandNb(request):
    qs = Site.objects.filter(site_type="command")
    return HttpResponse(len(qs))

@csrf_exempt
def date(request):
    return HttpResponse(datetime.now().year)


class fileUploadView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        fileForm = FileForm()
        #userUpdateForm = UserUpdateForm(request.POST)
        return render(request, 'api_v1/uploader.html', locals())

    def post(self, request):
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            filename = os.popen('ls media/files/').read().replace('\n','').replace(' ','')
            rename = str(datetime.now() ).replace(' ','')+ filename
            myCmd2 = os.popen('mv  media/files/'+filename+'   static/zimeooUserUpdateFiles/img/'+rename).read()
            imUrl = ImageUrl(url= 'static/zimeooUserUpdateFiles/img/'+rename, user = request.user )
            imUrl.save()
            return HttpResponse("{0}".format("success"))
        return HttpResponse('error')     
            

class img_uploaderViews(APIView):
    permission_classes = (IsAuthenticated,)
    
    def post(self, request):
        import os
        if request.method == 'POST' and request.is_ajax() :
            
            myfile = request.FILES['file']
            fs = FileSystemStorage()
            filename = fs.save('uploadImages/'+ request.user.username+'_'+ str(datetime.datetime.now()).replace(' ','_').replace(':','_')+myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            #request.user.username+'_'+ str(datetime.datetime.now()).replace(' ','_')+
            myCmd2 = os.popen('cd static/zimeooUserUpdateFiles/img/ & mkdir '+request.user.username+'' ).read()

            myCmd2 = os.popen('cp  media/'+filename+'   static/zimeooUserUpdateFiles/img/'+request.user.username+'/  &   rm media/'+filename+' ' ).read()
            
            return HttpResponse("{0}".format(uploaded_file_url))
        else:
            return HttpResponse("{0}".format('error'))


class command_cahier_chargeViews(APIView):
    permission_classes = (IsAuthenticated,)
    def post(request):
        if request.method == 'POST' and request.is_ajax() :
            myfile = request.FILES['file']
            fs = FileSystemStorage()
            filename = fs.save('media/commandFiles/'+ request.user.username+'_'+ str(datetime.datetime.now()).replace(' ','_').replace(':','_')+myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            #request.user.username+'_'+ str(datetime.datetime.now()).replace(' ','_')+
            
            return HttpResponse("{0}".format(uploaded_file_url))
        else:
            return HttpResponse("{0}".format('error'))


class partnerView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        #if Partner.objects.get(user  = request.user).exists() : return HttpResponse("true")
        #else: return HttpResponse("false")
        try: Partner.objects.get(user  = request.user)
        except ObjectDoesNotExist:   return HttpResponse("false")
        return HttpResponse("true")

    def post(self, request):
        newPartner = Partner(user = request.user)
        newPartner.save()
        return HttpResponse("success")


class partnerChildsView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        try: 
            p= Profile.objects.filter( parent = Partner.objects.get( user = User.objects.get(username="fayomihorace") ) )
            
            qs_json = serializers.serialize('json', p)
            profiles = eval(qs_json)
            users = []
            for p in profiles:
                nbDomain = Domain.objects.filter( user= User.objects.get( pk= p['fields']['user'] )  ).count()
                p['fields']['nbDomains'] = nbDomain
            return HttpResponse(json.dumps(profiles) , content_type='application/json')
        except ObjectDoesNotExist:   return HttpResponse("none")
       

class partnerPaidListView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self, request):
        p = PartnerGetMoney.objects.filter( partner = Partner.objects.get( user = request.user ) )
        qs_json = serializers.serialize( 'json', p )
        return HttpResponse( qs_json, content_type='application/json')
        
       