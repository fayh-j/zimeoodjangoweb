from django.contrib import admin
from django.urls import include, path
from api_v1 import  urls
from project import views


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index  ),
    path('v1.0/', include('api_v1.urls')),
]
