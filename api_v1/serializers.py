from django.contrib.auth.models import User
from api_v1.models import *
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class SiteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Site
        fields = ['name', 'description', 'price', 'slug', 'site_type']



class Site_categorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Site_category
        fields = ['name', 'description']
  

class WhatsappSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Whatsapp
        fields = ['user', 'phone']
   

class Site_templateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Site_template
        fields = ['name', 'templateName', 'description', 'site_category']


class DomainSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Domain
        fields = ['name', 'tld', 'payement_date', 'slug', 'site_type']
    
