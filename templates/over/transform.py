"""
By FAYOMI Horace
22/04/2019

"""
import sys

if  len(sys.argv) ==1:
	print("Error, give input file parameter!")
	exit()

def replacer(s, newstring, index, nofail=False):
    if not nofail and index not in range(len(s)):
        raise ValueError("index outside given string")
        return newstring + s
        return s + newstring
    return s[:index] + newstring + s[index + 1:]

def remove_at(i, s):
    return s[:i] + s[i+1:]

f = open(sys.argv[1], 'r+')

contents = f.read()	
contents = replacer(contents, '{% load static %}'+"\n"+contents[0] , 0, False) 
first= True
i=0
while i < len(contents):
	if contents[i]=='h' and contents[i+1]=='r' and contents[i+2]=='e' and contents[i+3]=='f' and contents[i+4]=='=':
		char = ""
		count=i+6
		while char =="":
			if contents[count]=='"':
				char='"'
				contents= replacer(contents,"'" +' %}" ', count , False)
			count+=1
			
		for j in range(	5 ):
			contents = remove_at(i+1, contents)
		contents = replacer(contents, 'href="{% static '+"'", i, False)
		#i=i+20

	
	elif contents[i]=='s' and contents[i+1]=='r' and contents[i+2]=='c' and contents[i+3]=='=':
		char = ""
		count=i+5
		while char =="":
			if contents[count]=='"':
				char='"'
				contents= replacer(contents,"'" +' %}" ', count , False)
			count+=1
			
		for j in range(	4 ):
			contents = remove_at(i+1, contents)
		contents = replacer(contents, 'src="{% static '+"'", i, False)
		#i=i+20
	i+=1

	
#print(contents)
f.close()
out = open(sys.argv[1]+".out", 'w')
out.write(contents)
out.close()
print("Output file named {} succefully generated! ".format(sys.argv[1]+".out"))

