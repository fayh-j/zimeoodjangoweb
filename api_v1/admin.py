from django.contrib import admin
from api_v1.models import *
# Register your models here.
class SiteAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name', ), }


admin.site.register(Site, SiteAdmin)
admin.site.register(Site_category)
admin.site.register(Site_template)
admin.site.register(Domain)
admin.site.register(Partner)
admin.site.register(Profile)
admin.site.register(Whatsapp)
admin.site.register(Tag)
admin.site.register(ImageUrl)
admin.site.register(PartnerGetMoney)

