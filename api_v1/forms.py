from django import forms
from  django.contrib.auth.models import User
from api_v1.models import *

class ConnexionForm(forms.Form):

    username = forms.CharField(label="Votre identifiant", max_length=30)
    #email = forms.EmailField(label="Votre adresse e-mail")

    password = forms.CharField(label="Votre mot de passe", widget=forms.PasswordInput)


class RegisterForm(forms.Form):

	firstname2 = forms.CharField(label="Nom", max_length=30)
	lastname2 = forms.CharField(label="Prénoms", max_length=30)
	username2 = forms.EmailField(label="Adresse e-mail")
	password2 = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
	password_conf2 = forms.CharField(label="Confirmer Mot de passe", widget=forms.PasswordInput)


class UserUpdateForm(forms.Form):
	#username = forms.EmailField(label="Nom d'utilisateur", required=False)
	email = forms.EmailField(label="Votre adresse e-mail", required=False)
	password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput, required=False)
	firstname = forms.CharField(label="Nom", max_length=30, required=False)
	lastname = forms.CharField(label="Prénoms", max_length=30, required=False)

	class Meta:
		model = User
		fields = [ 'email', 'password',  'firstname', 'lastname']
	

class FileForm(forms.ModelForm):
	class Meta:
		model = File
		fields = ('data',)