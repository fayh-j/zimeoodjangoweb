function refreshToken(first){
    console.log("refresh Token")
    //TRY to refresh token
    $.ajax({
      url: "/v1.0/user/login/token/refresh",
      method: "POST",
      data: {"refresh": $.cookie('zimeooUserRefresh') }
    }).done(function(r){
      var url = window.location.href.split('/')
      if ( url[url.length - 2 ] == "v1.0" ){
        console.log('return to dashborb')
        window.location.href = '/v1.0/user/dashboard';
      } 
            console.log('refreshhh  ok')
            $('#loginLi').remove();
            $('#showDashboard').removeClass('invisible');
            $('#user .name').html( ' <i class="fa fa-user"></i>&nbsp'+$.cookie('zimeooUser'));
            $.cookie('zimeooUserToken', r.access);
            //functionObject['executeFunction']()
            if (r.status == "no") $('#whatsappSuscribe').modal('show');
            

    }).fail(function() {
      var url = window.location.href.split('/')
      console.log('return to home')
      if ( url[url.length - 2 ] != "v1.0" ) window.location.href = '/v1.0';
    });
}

function refreshTokenDelay(delay){
  setInterval(function(){
    refreshToken(false)
  },delay);
}


$(function(){
  var u = window.location.href.split('?')
  if(u.length >1 ){
    var url= u[1].split('&')
    var token = url[1]
    var refresh = url[2]
    console.log( 'token : '+ token+'        refresh : '+refresh )
    $.cookie('zimeooUser', url[0] )
    $.cookie('zimeooUserToken', token);
    $.cookie('zimeooUserRefresh', refresh);
    var myNewURL = "/v1.0/user/dashboard";//the new URL
  var obj = { Title: "La première plateforme de création gratuite de sites web professionels dans la sous région", Url: myNewURL };      
    history.pushState(obj, obj.Title, obj.Url);   
  } //[1].split('&')
    
    refreshTokenDelay(5*60*60*1000)//5 hours
    checkPartner()

    $('.newSite').removeClass('invisible');
    $('#siteList td, #siteList th, #siteList td button ').css('text-align','center');
    $('#siteList td button').css('color','rgb(68, 68, 68)');
            $('#siteList tr').css('border-bottom','1px solid rgba(68, 68, 68, 0.2)');
            $('#siteList td button ').css('text-align','center').css('margin-top','7%');
            $('#siteList td button ').bind('mouseenter', function(){
                    $(this).css('color','white');
            }).bind('mouseleave', function(){
                    $(this).css('color','rgb(68, 68, 68)');
            }); 
    $('.newSite #newSiteBut' ).bind('click', function(){
      $('.CreateSite1').css('transition','all 2.5s').removeClass('invisible');
      $('.siteList, .newSite, .noSite').addClass('invisible');
      $('.title').text('Création de Site');
    });

    $('.CreateSite1   .prev').bind('click', function(){
      $('.CreateSite1').addClass('invisible');
      $('.siteList, .newSite, .noSite').removeClass('invisible');;
      $('.title').text('Sites & Commandes');
    });

    $('.CreateSite1 .site').bind('click', function(){
      $('.CreateSite1').addClass('invisible');
      $('.CreateSite2').removeClass('invisible');
      $('.title').text("Choisir votre Modèle" );
      $('.CreateSite2 .item').addClass('invisible');
      $('.CreateSite2 ').find('.'+$(this).find('.site').val()).removeClass('invisible');
      $('.CreateSite2 ').find('.'+$(this).val()).removeClass('invisible');
    });
    

    $('.CreateSite1   .command').bind('click', function(){
      /*$('.CreateSite1').addClass('invisible');
      $('.CommandSite').removeClass('invisible');
      $('.title').text("Commander un Site" );
      $('.siteName').focus();*/
      $('#infoModal').modal('show').find('.modal-body').html("Cette partie du site est en construction!!")
    });
    $('.CommandSite .prev').bind('click', function(){
      $('.CommandSite').addClass('invisible');
      $('.CreateSite1').removeClass('invisible');
    });
    
    $('.CreateSite2   .prev').bind('click', function(){
      $('.CreateSite2').addClass('invisible');
      $('.CreateSite1').removeClass('invisible');;
      $('.title').text('Sites & Commandes');
      $('.CreateSite2 .next').removeClass('invisible');
      $('.CreateSite2 .large-12').removeClass('invisible');
      $('.CreateSite2 .all').remove();
    });
    

    
    $('.mycard2').css('border','1px solid rgba(0, 0, 0, 0.1)').css('box-shadow','0px 0px 0px rgba(34,82,193, 0.5)');
    $('.mycard2 .card-body').css('border-radius','0px');
          $('.mycard2').bind('mouseenter', function(){
            $(this).css('box-shadow','0px 0px 5px rgba(34,82,193, 0.8)');
          }).bind('mouseleave', function(){
            $(this).css('box-shadow','0px 0px 0px rgba(34,82,193, 0.5)');
          });
 
    $('.CreateSite2 .next').bind('click', function(){
      var templates =  JSON.parse($.cookie('zimeooTemplates'));
     
      $('.CreateSite2 .all').removeClass('invisible');
      for( i= 0; i < templates.length; i++){
        var data = "static/img/templates/"+ templates[i].fields.name +".jpg";
        $('.CreateSite2 .large-12').after('<div class="item col-md-3 col-6 all"> <div class="box6"><img src="' + data + '"  alt=""><div class="box-content">  <span style="font-weight: bold; font-size: 1.5em" class="post">Modèle de '+ $.cookie("zimeooTemplatesType")+'</span> <ul class="icon"><li><button href="#"  class="btn btn-warning">Voir <i class="fa fa-eye"></i> </button></li><li><button href="#"  class="btn btn-warning">Choisir <i class="fa fa-check"></i> </button></li></ul></div></div></div>');
        console.log(templates[i]);
      }
      $('.CreateSite2 .large-12').addClass('invisible');
      $('.CreateSite2 .next').addClass('invisible');
    });

    $('#conf').click(function(){
      console.log('jjj'+  $(this).val());
      
      $('#deleteSiteModal .modal-footer button').attr('disabled', true);
      $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp Supression...')
    
            var data = {
                        siteID: $(this).val()
                      };
            
          $.ajax({
                url: "/v1.0/site",
                method: "DELETE",
                headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
                data: data,
                datatype: "json"
            }).done(function(response) {
              console.log( response);
              //$('.siteAfter').after('<div class="alert alert-successe text-center" id="siteError" role="alert">Votre site a été bien ajouté  !</div>');
              window.location.href = '/v1.0/user/dashboard';
            });
    });
   
    $('#siteModal .see').bind('click', function(){
      var link = '/static/templates/'+$(this).val() +'/distribution/index.html';
      var win = window.open( link, '_blank');
      if (win) {
          //Browser has allowed it to be opened
          win.focus();
      } else {
          //Browser has blocked it
          alert("S'il vous plait autorisez les  popups for this website");
      }
    });


    $('#siteModal .choice').bind('click', function(){
      $('#siteError').remove();
      
      if($('#siteName').val() =="" ){
        $('.siteAfter').after('<div class="alert alert-danger text-center" id="siteError" role="alert">Veuillez entrer un nom pour votre site !</div>');
      }
      else{

        $('#siteModal button, #deleteSiteModal input, #siteModal input').attr("disabled", true);
        $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp Patientez, Site en Création...')
          
            var data = {
                        type: "free",
                        username: $.cookie('zimeooUser'),
                        siteName: $('#siteName').val(),
                        template: $(this).val()
                      };
          $.ajax({
                url: "/v1.0/site",
                method: "POST",
                headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
                data: data,
                datatype: "json"
            }).done(function(response) {
              console.log( response);
              $('.waiting').addClass('invisible');
              $('#siteModal button, #deleteSiteModal input').attr("disabled", false);
      
              //$('.siteAfter').after('<div class="alert alert-successe text-center" id="siteError" role="alert">Votre site a été bien ajouté  !</div>');
              window.location.href = '/v1.0/user/dashboard';
            });
        }
     
    });
 
    //$('.myValidation form').on('submit',function(e){
    $('#sendCommand').click(function(e){
      e.preventDefault();
      //$('.CommandSite .send, .CommandSite input,.CommandSite textarea ').attr("disabled", true);
      var description = $('.myValidation .description').val();
      var siteName = $('.myValidation .siteName').val();
      console.log( siteName );
      if (description.length<10 || siteName.length <3){
        $('.CommandSite .alert').removeClass('invisible');
      }else{
        $('.CommandSite .alert').addClass('invisible');
      $('.CommandSite .waiting').removeClass('invisible');
        var fileInput = $('.commandFile').prop('files');
        console.log( $('.commandFile').prop('files') );
        if( fileInput != null ){
                  var file = fileInput[0];
                  var formData = new FormData();
                  formData.append('file', file);
                  console.log(formData);
                  $.ajax({
                      url: 'http://127.0.0.1:8000/uploads/command_cahier_charge/',
                      type: 'POST',
                      headers: {'X-CSRFToken': $.cookie('zimeooUserToken')},
                      data: formData,
                      async: true,
                      cache: false,
                      contentType: false,
                      enctype: 'multipart/form-data',
                      processData: false,
                      beforeSend: function() {
                          console.log('Uploading...');
                          $('.upload-progress').show();
                      },
                      success: function (response) {
                        $.cookie('zimeooCommandFilePath',response);
                      },
                      error: function(response) {
                        $.cookie('zimeooCommandFilePath',"");
                        
                      }
                  });
                }
                
                            var data = {
                                        type: "command",
                                        username: $.cookie('zimeooUser'),
                                        siteName: siteName,
                                        description: description,
                                      };
                          request = $.ajax({
                                url: "/v1.0/site",
                                method: "POST",
                                headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
                                data: data,
                                datatype: "json"
                            });
                            request.done(function(response) {
                              console.log( response);
                              $('.waiting').addClass('invisible');
                              $('.CommandSite .send, .CommandSite input,.CommandSite textarea ').attr("disabled", false);
                              //$('.siteAfter').after('<div class="alert alert-successe text-center" id="siteError" role="alert">Votre site a été bien ajouté  !</div>');
                              window.location.href = '/v1.0/user/dashboard';
                            });
        
      }
      //var file
      //on envoie d'ab les fichiers si il en a
      
      });

    $('#updateModal').on('shown.bs.modal', function() {
              $(this).find('label, .helptext').remove();
              $(this).find('input').css('border-radius','5px').css('width','100%').addClass('form-control');
              $(this).find('#id_firstname').attr('placeholder','Prénom');
              $(this).find('#id_lastname').attr('placeholder','Nom');
              $(this).find('#id_email').attr('placeholder','Email');
              $(this).find('#id_username').attr('placeholder',"Nom d'utilisateur");
              $(this).find('#id_password').attr('placeholder','Mot de passe');
              $(this).find('#id_password_conf').attr('placeholder','Confirmation de Mot de passe');
    });

    $('#addImgModal #upload').click(function(){
        $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp Uploading...').attr('disabled', true)
        console.log('upload')
        var data = new FormData($('#uploader').get(0));
      
          $.ajax({
              url: '/v1.0/uploads' ,
              type: "POST",
              data: data,
              cache: false,
              headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
              processData: false,
              contentType: false,
              success: function(data) {
                console.log('upload end')
                $('#addImgModal #upload').html('Uploader').attr('disabled', false)
                $('#addImgModal').modal('hide')
              }
          })
    });

    $('#addImgModal ').on('hidden.bs.modal', function (e) {
      $('#addImgModal #upload').html('Uploader').attr('disabled', false)
      $('#modifySiteModal').modal('show')
    })
    $('#siteModal').on('hidden.bs.modal', function (e) {
      //$('#siteModal').modal('show');
    })
    $('#siteModal .siteName').click(function(){
      $('#siteModal #siteName').focus()
    })
    
    $('#becomePartnerButton').click(function(){
      $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp Patientez...').attr('disabled',true)
      
        $.ajax({
          url: "/v1.0/user/partner/become",
          headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
          method: "POST"
        }).done(function(r){
          console.log(r)
          window.location.href  = window.location.href 
        }).fail(function() {
          $(this).html('Continuer').attr('disabled',false)
        });
    })

    $('#deleteModal .conf').click(function(){
        $('#deleteModal .modal-footer button').attr('disabled', true);
        $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> &nbsp Supression...')
    
           var data = { path: $(this).attr('id'),};
          $.ajax({
                url: "/v1.0/site/images/delete",
                method: "DELETE",
                headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
                data: data,
                datatype: "json"
            }).done(function(response) {
              console.log( response);
              //$('.siteAfter').after('<div class="alert alert-successe text-center" id="siteError" role="alert">Votre site a été bien ajouté  !</div>');
              $(this).html('Confirmer')
              $('#deleteModal .modal-footer button').attr('disabled', false);
              $('#deleteModal').modal('hide')
            });
            $(this).html('Confirmer')
            $('#deleteModal .modal-footer button').attr('disabled', false);
    });
    $('#deleteModal').on('hidden.bs.modal', function (e) {$('#modifySiteModal').modal('show')})

    $('#becomePartnerModal').on('shown.bs.modal', function() {
                $('.mobile-nav-overly, .mobile-nav, .mobile-nav-toggle').css('display','none');
                $(this).find('label').remove();
            }).on('hidden.bs.modal', function() {
                $(' .mobile-nav, .mobile-nav-toggle').css('display','block');
            });

    //disable rigth click
    /*$(document).on("contextmenu",function(e){
      e.preventDefault();
   });*/
    
    
    //alert('yeahooooooooooooooooooo');
});

function checkPartner(){
                $.ajax({
                  url: "/v1.0/user/partner/checkIfExist",
                  headers: {"Authorization": "Bearer "+$.cookie('zimeooUserToken')},
                  method: "GET"
                }).done(function(r){
                  console.log('is partner : '+r)
                  if( r =='true' ){
                    $('#becomePartnerOpenModal').remove()
                    $('#showPartner').removeClass('invisible')
                    var link = 'https://zimeoo.com/partner?'+ $.cookie('zimeooUser')
                    $('#partnerLink').val(link )
                    $('#copyPartnerLink').click(function(e) {
                      $('#copyPartnerLink').attr('title','Lien copié').tooltip('show');
                       $('#partnerLink').focus().select();
                       document.execCommand('copy');
                       //alert('what to copy')
                     });
                  }  
                }).fail(function() {
                  
                });
            
} 